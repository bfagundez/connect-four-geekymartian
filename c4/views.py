from flask import render_template, request, jsonify
from c4 import c4
import numpy as np

WINNER = None

GAMEBOARD = [ ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'] ]


PLAYER1 = 'A'
PLAYER2 = 'B'
CURRENT_TURN = PLAYER1
CURRENT_PLAYER = None

@c4.route('/')
def index():
  return render_template('instructions.html')

@c4.route('/api/returnboard')
def returnBoard():
  global WINNER
  return render_template('board.html',gameBoard=GAMEBOARD, winner=WINNER, currentTurn=CURRENT_TURN, currentPlayer=CURRENT_PLAYER)

@c4.route('/users/1')
def player1():

  return render_template('index.html',welcome='welcome player 1',gameBoard=GAMEBOARD,playerid=PLAYER1,winner=WINNER)

@c4.route('/users/2')
def player2():
  return render_template('index.html',welcome='welcome player 2',gameBoard=GAMEBOARD,playerid=PLAYER2,winner=WINNER)

@c4.route('/api/resetboard')
def resetBoard():
  global GAMEBOARD
  global WINNER
  GAMEBOARD = [
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'],
              ['_','_','_','_','_','_','_'] ]
  WINNER = None
  return 'ok!'

@c4.route('/api/updateboard', methods = ['POST'])
def updateBoard():

  global WINNER
  global CURRENT_TURN
  payload = {}

  col = int(request.form['col'])
  player = request.form['player']

  if PLAYER1 == CURRENT_TURN:
    CURRENT_TURN = PLAYER2
  elif PLAYER2 == CURRENT_TURN:
    CURRENT_TURN = PLAYER1

  print 'col: '+str(col)
  print 'player: '+str(player)

  for row in reversed(GAMEBOARD):
    if row[col] == '_':
      row[col] = player
      break

  payload["success"] = True

  WINNER = checkboardforwinners()

  return jsonify(payload)

# check for winners
def checkboardforwinners():
  global PLAYER1
  global PLAYER2

  # check in horizontal, easy!
  for row in reversed(GAMEBOARD):
    player1count = 0
    player2count = 0

    for cell in row:
      if cell != '_':
        if cell == PLAYER1:
          player1count += 1

        if cell == PLAYER2:
          player2count += 1

        if player1count >= 4:
          return PLAYER1

        if player2count >= 4:
          return PLAYER2

  # check in vertical easy 2!
  player1colscount = {0:0,1:0,2:0,3:0,4:0,5:0,6:0,7:0}
  player2colscount = {0:0,1:0,2:0,3:0,4:0,5:0,6:0,7:0}

  for row in reversed(GAMEBOARD):
    for i, cell in enumerate(row):
      if cell != '_':
        if cell == PLAYER1:
          player1colscount[i] += 1
        if cell == PLAYER2:
          player2colscount[i] += 1

  for col in player1colscount:
    if player1colscount[col] >= 4:
      return PLAYER1

  for col in player2colscount:
    if player2colscount[col] >= 4:
      return PLAYER2

  # check in diagonals (numpy help here)
  x,y = 7,6

  matrix = np.array(GAMEBOARD)
  diags = [matrix[::-1,:].diagonal(i) for i in range(-matrix.shape[0]+1,matrix.shape[1])]
  diags.extend(matrix.diagonal(i) for i in range(matrix.shape[1]-1,-matrix.shape[0],-1))

  for diag in diags:
    player1counter = 0
    player2counter = 0
    for item in diag:
      if item != '_':
        if item == PLAYER1:
          player1counter += 1
        if item == PLAYER2:
          player2counter += 1
      if player1counter >= 4:
        return PLAYER1
      if player2counter >= 4:
        return PLAYER2

  return None


