$(function(){
  console.log('Hello world!');

  var dataRef = new Firebase('https://incandescent-fire-7005.firebaseio.com/gameboardupdates');

  $('#resetBoard').click(function(){
      if(confirm('Are you sure?')){
        $.get( "/api/resetboard",function(data){
          refreshBoard();
          dataRef.push('Board reset');
        });
      }
    });

  var updateBoard = function(movedata){
    $.post( "/api/updateboard",movedata, function( data ) {
      if(data.success && data.winnerfound){
        refreshBoard();
        dataRef.push(data.winner);
      } else if(data.success){
        refreshBoard();
        dataRef.push('player moved');
      }
    });
  };

  var refreshBoard = function() {
    $.get( '/api/returnboard',function(data){
        $('#gameboard').html(data);
        assignBehavior();
    })
  };


  var assignBehavior = function(){
    var playerid = $('#playerid').text();
    if(playerid != $('#currentturn').text()){
      $('.triggerrow').hide();
    }

    if(playerid == 'B'){
      $('.coltrigger .circle').addClass('red');
    }
    if(playerid =='A'){
      $('.coltrigger .circle').addClass('blue');
    }

    $('.coltrigger').click(function(){
      var $matrixSlot = $(this);
      var collnumber = $matrixSlot.data('colnumber');
      var playerid = $('#playerid').text();
      updateBoard({ player:playerid,col:collnumber});
    });

    $('.resetBoard').click(function(){
      if(confirm('Are you sure?')){
        $.get( "/api/resetboard",function(data){
          refreshBoard();
          dataRef.push('Board reset');
        });
      }
    });


  }

  assignBehavior();

  dataRef.on("value", function(snapshot) {
    console.log('updating board!');
    refreshBoard();
  });

});